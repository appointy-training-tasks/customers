export default interface Customer {
  id: string;
  name: string;
  email: string;
  details: {
    phoneNumber: number;
    company: string;
    address: string;
    language: "English" | "Tamil" | "Hindi";
    timezone: string;
  };
  specialDates: [String, Date][];
}
