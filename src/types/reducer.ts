import Customer from "./customer";

export type ReducerState = {
  customers: Customer[];
  selectedCustomer: Customer | null;
};

export type ReducerAction =
  | {
      type: "customers/customerAdded";
      payload: Customer;
    }
  | {
      type: "customers/customerUpdated";
      payload: Customer;
    }
  | {
      type: "customers/customerDeleted";
      payload: Customer["id"];
    }
  | {
      type: "selectedCustomer";
      payload: Customer | null;
    };
