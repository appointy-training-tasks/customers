import { v4 as uuid } from "uuid";

import Customer from "../types/customer";

export const createDummyCustomer = (index?: number): Customer => ({
  id: uuid(),
  name: `Customer ${index || "Name"}`,
  email: `customer${index || ""}@example.com`,
  details: {
    phoneNumber: 9812345678,
    company: "Customer Company",
    address: "Customer Address",
    language: "English",
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  },
  specialDates: [],
});

export const createCustomer = (
  name: string,
  email: string,
  phoneNumber: number
): Customer => ({
  id: uuid(),
  name,
  email,
  details: {
    phoneNumber,
    company: "",
    address: "",
    language: "English",
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  },
  specialDates: [],
});
