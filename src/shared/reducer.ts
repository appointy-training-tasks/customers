import { Reducer } from "react";
import { ReducerAction, ReducerState } from "../types/reducer";
import { createDummyCustomer } from "./customer";

export const initialReducerState = {
  customers: [createDummyCustomer(1), createDummyCustomer(2)],
  selectedCustomer: null,
};

const reducer: Reducer<ReducerState, ReducerAction> = (
  state = initialReducerState,
  action
) => {
  switch (action.type) {
    case "customers/customerAdded":
      return {
        ...state,
        customers: [...state.customers, action.payload],
      };

    case "customers/customerUpdated":
      return {
        ...state,
        customers: state.customers.map((customer) =>
          action.payload.id === customer.id ? action.payload : customer
        ),
      };

    case "customers/customerDeleted":
      return {
        ...state,
        customers: state.customers.filter(
          (customer) => customer.id !== action.payload
        ),
      };

    case "selectedCustomer":
      return {
        ...state,
        selectedCustomer: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
