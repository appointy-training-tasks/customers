import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Dialog } from "@mui/material";
import AddCustomerDialog from "../components/Dialogs/AddCustomerDialog";
import DeleteCustomerDialog from "../components/Dialogs/DeleteCustomerDialog";
import UpdateCustomerDialog from "../components/Dialogs/UpdateCustomerDialog";
import { createDummyCustomer } from "../shared/customer";

export default {
  title: "Customer/Dialogs",
  component: Dialog,
} as ComponentMeta<typeof Dialog>;

export const AddCustomer: ComponentStory<typeof AddCustomerDialog> = (args) => (
  <AddCustomerDialog {...args} />
);
AddCustomer.args = {
  open: true,
};

export const DeleteCustomer: ComponentStory<typeof DeleteCustomerDialog> = (
  args
) => <DeleteCustomerDialog {...args} />;
DeleteCustomer.args = {
  open: true,
};

export const UpdateCustomer: ComponentStory<typeof UpdateCustomerDialog> = (
  args
) => <UpdateCustomerDialog {...args} />;
UpdateCustomer.args = {
  open: true,
  customer: createDummyCustomer(),
};
