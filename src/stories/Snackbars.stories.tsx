import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import CustomSnackBar from "../components/SnackBar/CustomSnackBar";

export default {
  title: "Customer/SnackBars",
  component: CustomSnackBar,
} as ComponentMeta<typeof CustomSnackBar>;

const Template: ComponentStory<typeof CustomSnackBar> = (args) => (
  <CustomSnackBar {...args} />
);
export const AddCustomer = Template.bind({});
AddCustomer.args = {
  open: true,
  severity: "success",
  message: "Customer added successfully!",
};

export const DeleteCustomer = Template.bind({});
DeleteCustomer.args = {
  open: true,
  severity: "info",
  message: "Customer deleted successfully!",
};

export const UpdateCustomer = Template.bind({});
UpdateCustomer.args = {
  open: true,
  severity: "info",
  message: "Customer updated successfully!",
};
