import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import CustomerDetails from "../components/CustomerDetails/CustomerDetails";
import { createDummyCustomer } from "../shared/customer";

export default {
  title: "Customer/Details",
  component: CustomerDetails,
} as ComponentMeta<typeof CustomerDetails>;

const Template: ComponentStory<typeof CustomerDetails> = (args) => (
  <CustomerDetails {...args} />
);
export const Details = Template.bind({});
Details.args = {
  customer: createDummyCustomer(1),
};
