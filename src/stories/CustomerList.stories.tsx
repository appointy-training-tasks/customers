import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import CustomerList from "../components/CustomerList/CustomerList";
import { createDummyCustomer } from "../shared/customer";

export default {
  title: "Customer/List",
  component: CustomerList,
} as ComponentMeta<typeof CustomerList>;

const Template: ComponentStory<typeof CustomerList> = (args) => (
  <CustomerList {...args} />
);
export const List = Template.bind({});
List.args = {
  customers: [createDummyCustomer(1), createDummyCustomer(2)],
};
