import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import CustomerListButton from "../components/CustomerList/CustomerListButton";
import { createDummyCustomer } from "../shared/customer";

export default {
  title: "Customer/List/Button",
  component: CustomerListButton,
} as ComponentMeta<typeof CustomerListButton>;

const Template: ComponentStory<typeof CustomerListButton> = (args) => (
  <CustomerListButton {...args} />
);
export const Button = Template.bind({});
const customer = createDummyCustomer(1);
Button.args = {
  id: customer.id,
  name: customer.name,
  email: customer.email,
};
