import React from "react";

import { ComponentMeta } from "@storybook/react";
import App from "../components/App/App";

export default {
  title: "Customer/Dashboard",
  component: App,
} as ComponentMeta<typeof App>;

export const Dashboard = () => <App />;
