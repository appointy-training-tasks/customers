import { Close } from "@mui/icons-material";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import Customer from "../../types/customer";
import { createCustomer } from "../../shared/customer";

interface Props {
  open: boolean;
  onClose: () => void;
  onAddCustomer: (customer: Customer) => void;
}

interface CustomerInfo {
  name: string;
  email: string;
  phoneNumber: number;
}

const AddCustomerDialog: React.FC<Props> = ({
  open,
  onClose,
  onAddCustomer,
}) => {
  const [newCustomer, setNewCustomer] = useState<CustomerInfo>({
    name: "",
    email: "",
    phoneNumber: 0,
  });

  const onNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewCustomer((newCustomer) => ({ ...newCustomer, name: e.target.value }));
  };

  const onEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewCustomer((newCustomer) => ({
      ...newCustomer,
      email: e.target.value,
    }));
  };

  const onPhoneNumberChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewCustomer((newCustomer) => ({
      ...newCustomer,
      phoneNumber: Number(e.target.value),
    }));
  };

  const onSave = () => {
    onAddCustomer(
      createCustomer(
        newCustomer.name,
        newCustomer.email,
        newCustomer.phoneNumber
      )
    );
    clearCustomerInfo();
  };

  const onCancel = () => {
    clearCustomerInfo();
    onClose();
  };

  const clearCustomerInfo = () => {
    setNewCustomer({ name: "", email: "", phoneNumber: 0 });
  };

  return (
    <Dialog open={open} onClose={onCancel}>
      <DialogTitle>
        <Grid container justifyContent="flex-end">
          <IconButton onClick={onCancel}>
            <Close />
          </IconButton>
        </Grid>
        <Typography variant="h6" component="p" sx={{ marginLeft: "2rem" }}>
          New Customer
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ padding: "4rem" }}>
        <TextField
          placeholder="Name"
          variant="standard"
          fullWidth
          margin="dense"
          value={newCustomer.name}
          onChange={onNameChange}
        />
        <TextField
          placeholder="Email"
          variant="standard"
          fullWidth
          margin="dense"
          value={newCustomer.email}
          onChange={onEmailChange}
        />
        <TextField
          variant="standard"
          type="number"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <img
                  alt="india"
                  src="https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg"
                  style={{ width: "24px", height: "24px", marginRight: "5px" }}
                />
                +91
              </InputAdornment>
            ),
          }}
          fullWidth
          margin="dense"
          value={newCustomer.phoneNumber}
          onChange={onPhoneNumberChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>CANCEL</Button>
        <Button variant="contained" onClick={() => onSave()}>
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddCustomerDialog;
