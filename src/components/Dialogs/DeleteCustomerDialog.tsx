import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import React from "react";

interface Props {
  open: boolean;
  onClose: () => void;
  onDeleteCustomer: () => void;
}

const DeleteCustomerDialog: React.FC<Props> = ({
  open,
  onClose,
  onDeleteCustomer,
}) => {
  return (
    <Dialog open={open} onClose={onClose} fullWidth>
      <DialogTitle>
        <Typography variant="h6" component="h2">
          Delete Customer
        </Typography>
      </DialogTitle>
      <DialogContent>Are you sure you want to continue?</DialogContent>
      <DialogActions>
        <Button onClick={onClose}>CANCEL</Button>
        <Button variant="contained" onClick={() => onDeleteCustomer()}>
          CONFIRM
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteCustomerDialog;
