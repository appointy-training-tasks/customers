import { Close } from "@mui/icons-material";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  TextField,
  Typography,
} from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import Customer from "../../types/customer";

interface Props {
  open: boolean;
  customer: Customer | null;
  onClose: () => void;
  onUpdateCustomer: (customer: Customer) => void;
}

const UpdateCustomerDialog: React.FC<Props> = ({
  open,
  customer,
  onClose,
  onUpdateCustomer,
}) => {
  const [customerInfo, setCustomerInfo] = useState<Customer | null>(customer);
  const resetCustomerInfo = useCallback(() => {
    setCustomerInfo(customer ? { ...customer } : null);
  }, [customer]);

  useEffect(() => {
    resetCustomerInfo();
  }, [customer, resetCustomerInfo]);

  const onCustomerAttributeChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setCustomerInfo((customerInfo) =>
      customerInfo
        ? {
            ...customerInfo,
            [e.target.name]: e.target.value,
          }
        : customerInfo
    );
  };

  const onCustomerDetailsChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCustomerInfo((customerInfo) =>
      customerInfo
        ? {
            ...customerInfo,
            details: {
              ...customerInfo.details,
              [e.target.name]: e.target.value,
            },
          }
        : customerInfo
    );
  };

  const onSave = () => {
    if (customerInfo) onUpdateCustomer(customerInfo);
    resetCustomerInfo();
  };

  const onCancel = () => {
    onClose();
    resetCustomerInfo();
  };

  return (
    <Dialog open={open} onClose={onCancel}>
      <DialogTitle>
        <Grid container justifyContent="flex-end">
          <IconButton onClick={onCancel}>
            <Close />
          </IconButton>
        </Grid>
        <Typography variant="h6" component="h3" sx={{ marginLeft: "2rem" }}>
          Update Customer
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ padding: "4rem" }}>
        <TextField
          placeholder="Name"
          variant="standard"
          fullWidth
          margin="dense"
          name="name"
          value={customerInfo?.name || ""}
          onChange={onCustomerAttributeChange}
        />
        <TextField
          placeholder="Email"
          variant="standard"
          fullWidth
          margin="dense"
          name="email"
          value={customerInfo?.email || ""}
          onChange={onCustomerAttributeChange}
        />

        <Typography
          variant="subtitle1"
          component="h4"
          sx={{ marginTop: "2rem", color: "text.secondary" }}>
          Details
        </Typography>
        <TextField
          placeholder="Company"
          variant="standard"
          fullWidth
          margin="dense"
          name="company"
          value={customerInfo?.details.company || ""}
          onChange={onCustomerDetailsChange}
        />
        <TextField
          placeholder="Address"
          variant="standard"
          fullWidth
          margin="dense"
          name="address"
          value={customerInfo?.details.address || ""}
          onChange={onCustomerDetailsChange}
        />
        <TextField
          variant="standard"
          type="number"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <img
                  alt="india"
                  src="https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg"
                  style={{ width: "24px", height: "24px", marginRight: "5px" }}
                />
                +91
              </InputAdornment>
            ),
          }}
          fullWidth
          margin="dense"
          name="phoneNumber"
          value={customerInfo?.details.phoneNumber || 0}
          onChange={onCustomerDetailsChange}
        />
        <TextField
          placeholder="Language"
          variant="standard"
          fullWidth
          margin="dense"
          select
          name="language"
          value={customerInfo?.details.language || ""}
          onChange={onCustomerDetailsChange}>
          {["English", "Tamil", "Hindi"].map((language) => (
            <MenuItem key={language} value={language}>
              {language}
            </MenuItem>
          ))}
        </TextField>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>CANCEL</Button>
        <Button variant="contained" onClick={() => onSave()}>
          SAVE
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UpdateCustomerDialog;
