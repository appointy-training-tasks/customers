import {
  Avatar,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import React from "react";
import Customer from "../../types/customer";

interface Props {
  id: Customer["id"];
  name: Customer["name"];
  email: Customer["email"];
  onCustomerListItemClick: () => void;
}

const CustomerListButton: React.FC<Props> = ({
  id,
  name,
  email,
  onCustomerListItemClick,
}) => {
  return (
    <ListItemButton alignItems="center" onClick={onCustomerListItemClick}>
      <ListItemAvatar>
        <Avatar alt={name} src={`https://i.pravatar.cc/150?u=${id}`} />
      </ListItemAvatar>
      <ListItemText primary={name} secondary={email} />
    </ListItemButton>
  );
};

export default React.memo(CustomerListButton);
