import { Divider, List } from "@mui/material";
import React from "react";
import Customer from "../../types/customer";
import CustomerDetailButton from "./CustomerListButton";

interface Props {
  customers: Customer[];
  onCustomerListItemClick: (customer: Customer) => void;
}

const CustomerList: React.FC<Props> = ({
  customers,
  onCustomerListItemClick,
}) => {
  return (
    <List>
      {customers.map((customer, index) => (
        <div key={customer.id}>
          <CustomerDetailButton
            id={customer.id}
            name={customer.name}
            email={customer.email}
            onCustomerListItemClick={() => onCustomerListItemClick(customer)}
          />
          {index !== customers.length - 1 && <Divider component="li" />}
        </div>
      ))}
    </List>
  );
};

export default React.memo(CustomerList);
