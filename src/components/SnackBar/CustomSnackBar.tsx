import { Alert, Button, Snackbar } from "@mui/material";
import React from "react";

export interface SnackBarProps {
  open: boolean;
  onClose: () => void;
  severity: "error" | "success" | "info" | "warning";
  message: string;
}

const CustomSnackBar: React.FC<SnackBarProps> = ({
  open,
  onClose,
  severity,
  message,
}) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={onClose}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}>
      <Alert
        severity={severity}
        variant="filled"
        sx={{ width: "100%" }}
        action={
          <Button color="inherit" size="small" onClick={onClose}>
            DISMISS
          </Button>
        }>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default React.memo(CustomSnackBar);
