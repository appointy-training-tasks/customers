import { Close, DeleteOutlined, EditOutlined } from "@mui/icons-material";
import {
  Avatar,
  Card,
  CardHeader,
  Grid,
  IconButton,
  Paper,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import Customer from "../../types/customer";

interface Props {
  customer: Customer | null;
  onClose: () => void;
  onUpdateCustomer: () => void;
  onDeleteCustomer: () => void;
}

const CustomerDetails: React.FC<Props> = ({
  customer,
  onClose,
  onUpdateCustomer,
  onDeleteCustomer,
}) => {
  const styles = {
    details: {
      color: "text.secondary",
      marginBottom: "10px",
    },
    detailsValue: {
      color: "text.primary",
      marginBottom: "10px",
    },
    categories: { marginBottom: "10px", fontSize: "1.1rem", marginLeft: "5px" },
  };

  return (
    <Box sx={{ padding: "1rem" }}>
      <Grid container justifyContent={"flex-end"}>
        <IconButton onClick={onUpdateCustomer}>
          <EditOutlined />
        </IconButton>
        <IconButton onClick={onDeleteCustomer}>
          <DeleteOutlined />
        </IconButton>
        <IconButton onClick={onClose}>
          <Close />
        </IconButton>
      </Grid>
      <Card sx={{ marginBottom: "30px", boxShadow: 0 }}>
        <CardHeader
          avatar={
            <Avatar
              alt={customer?.name || "Customer Name"}
              src={`https://i.pravatar.cc/150?u=${customer?.id || "0"}`}
            />
          }
          title={
            <Typography variant="h5">
              {customer?.name || "Customer Name"}
            </Typography>
          }
          subheader={customer?.email || "customer@example.com"}
        />
      </Card>
      <Box sx={{ padding: "1.6rem", margin: "1rem" }}>
        <Typography variant="h6" color="text.secondary" sx={styles.categories}>
          DETAILS
        </Typography>
        <Paper sx={{ padding: "1rem 2rem" }}>
          <Grid container>
            <Grid item xs={4} sx={styles.details}>
              Company
            </Grid>
            <Grid item xs={8} sx={styles.detailsValue}>
              {customer?.details?.company || ""}
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={4} sx={styles.details}>
              Address
            </Grid>
            <Grid item xs={8} sx={styles.detailsValue}>
              {customer?.details?.address || ""}
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={4} sx={styles.details}>
              Telephone
            </Grid>
            <Grid item xs={8} sx={styles.detailsValue}>
              {customer?.details?.phoneNumber || ""}
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={4} sx={styles.details}>
              Language
            </Grid>
            <Grid item xs={8} sx={styles.detailsValue}>
              {customer?.details?.language || ""}
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={4} sx={styles.details}>
              Timezone
            </Grid>
            <Grid item xs={8} sx={styles.detailsValue}>
              {customer?.details?.timezone || ""}
            </Grid>
          </Grid>
        </Paper>
      </Box>
      <Box sx={{ padding: "2rem", margin: "1rem" }}>
        <Typography variant="h6" color="text.secondary" sx={styles.categories}>
          SPECIAL DATES
        </Typography>
        <Paper sx={{ padding: "1rem 2rem" }}>
          {customer?.specialDates && customer?.specialDates.length > 0 ? (
            customer.specialDates.map(([dateName, date]) => (
              <Grid container>
                <Grid item xs={4} sx={styles.details}>
                  {dateName}
                </Grid>
                <Grid item xs={8} sx={styles.details}>
                  {date.toString()}
                </Grid>
              </Grid>
            ))
          ) : (
            <Grid container sx={styles.detailsValue}>
              No special dates added.
            </Grid>
          )}
        </Paper>
      </Box>
    </Box>
  );
};

export default CustomerDetails;
