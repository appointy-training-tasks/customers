import { Add, Search } from "@mui/icons-material";
import {
  AppBar,
  Drawer,
  IconButton,
  InputAdornment,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback, useEffect, useReducer, useState } from "react";
import { useDebounce } from "use-debounce";
import reducer, { initialReducerState } from "../../shared/reducer";
import Customer from "../../types/customer";
import CustomerDetails from "../CustomerDetails/CustomerDetails";
import CustomerList from "../CustomerList/CustomerList";
import AddCustomerDialog from "../Dialogs/AddCustomerDialog";
import DeleteCustomerDialog from "../Dialogs/DeleteCustomerDialog";
import UpdateCustomerDialog from "../Dialogs/UpdateCustomerDialog";
import CustomSnackBar, { SnackBarProps } from "../SnackBar/CustomSnackBar";

import "./App.css";

const App: React.FC = () => {
  const [store, dispatch] = useReducer(reducer, initialReducerState);
  const [customers, setCustomers] = useState(store.customers);
  const [searchQuery, setSearchQuery] = useState("");

  const [customerDetailsOpen, setCustomerDetailsOpen] = useState(false);
  const [addCustomerDialogOpen, setAddCustomerDialogOpen] = useState(false);
  const [updateCustomerDialogOpen, setUpdateCustomerDialogOpen] =
    useState(false);
  const [deleteCustomerDialogOpen, setDeleteCustomerDialogOpen] =
    useState(false);
  const [snackBarState, setSnackBarState] = useState<
    Omit<SnackBarProps, "onClose">
  >({
    open: false,
    severity: "success",
    message: "",
  });

  const debouncedSearchQuery = useDebounce(searchQuery, 500)[0];

  const searchCustomers = useCallback(
    () =>
      store.customers.filter((customer) =>
        customer.name.includes(debouncedSearchQuery.trim())
      ),
    [store.customers, debouncedSearchQuery]
  );

  // fire only when the customer store changes or the debounced search query changes
  useEffect(() => {
    const matchedCustomers = searchCustomers();
    setCustomers(matchedCustomers);
  }, [store.customers, debouncedSearchQuery, searchCustomers]);

  // Event Handlers
  const onSearchQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(e.target.value);
  };

  const onToggleCustomerDetailsOpen = () => {
    setCustomerDetailsOpen((customerDetailsOpen) => !customerDetailsOpen);
  };

  const onCloseSnackBar = () =>
    setSnackBarState((snackBarState) => ({
      ...snackBarState,
      open: false,
    }));

  // Utility functions
  const dispatchSelectedCustomer = (customer: Customer | null) => {
    dispatch({ type: "selectedCustomer", payload: customer });

    if (customer !== null) setCustomerDetailsOpen(true);
  };

  const dispatchAddCustomer = (customer: Customer) => {
    dispatch({ type: "customers/customerAdded", payload: customer });
    setAddCustomerDialogOpen(false);
    setSnackBarState({
      open: true,
      severity: "success",
      message: "Customer added successfully!",
    });
  };

  const dispatchUpdateCustomer = (customer: Customer) => {
    dispatch({ type: "customers/customerUpdated", payload: customer });
    dispatch({ type: "selectedCustomer", payload: customer });
    setUpdateCustomerDialogOpen(false);
    setSnackBarState({
      open: true,
      severity: "info",
      message: "Customer updated successfully!",
    });
  };

  const dispatchDeleteCustomer = () => {
    if (store.selectedCustomer === null) return;

    dispatch({
      type: "customers/customerDeleted",
      payload: store.selectedCustomer.id,
    });
    dispatch({ type: "selectedCustomer", payload: null });
    setCustomerDetailsOpen(false);
    setDeleteCustomerDialogOpen(false);
    setSnackBarState({
      open: true,
      severity: "info",
      message: "Customer deleted successfully!",
    });
  };

  return (
    <Box sx={{ padding: "1rem 2rem" }}>
      <AppBar position="relative" sx={{ boxShadow: 0 }} color="transparent">
        <Toolbar>
          <Typography variant="h4" component="h1" sx={{ flex: 1 }}>
            Customers
          </Typography>
          <TextField
            variant="outlined"
            placeholder="Type to search..."
            size="small"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              ),
              sx: { borderRadius: "2em" },
            }}
            onChange={onSearchQueryChange}
          />
          <IconButton
            sx={{ marginLeft: "20px" }}
            onClick={() => setAddCustomerDialogOpen(true)}>
            <Add />
          </IconButton>
        </Toolbar>
      </AppBar>
      <CustomerList
        customers={customers}
        onCustomerListItemClick={dispatchSelectedCustomer}
      />

      <Drawer
        anchor="right"
        PaperProps={{ sx: { width: "50%" } }}
        open={customerDetailsOpen}
        onClose={onToggleCustomerDetailsOpen}>
        <CustomerDetails
          customer={store.selectedCustomer}
          onClose={onToggleCustomerDetailsOpen}
          onUpdateCustomer={() => setUpdateCustomerDialogOpen(true)}
          onDeleteCustomer={() => setDeleteCustomerDialogOpen(true)}
        />
      </Drawer>

      <AddCustomerDialog
        open={addCustomerDialogOpen}
        onClose={() => setAddCustomerDialogOpen(false)}
        onAddCustomer={dispatchAddCustomer}
      />
      <UpdateCustomerDialog
        open={updateCustomerDialogOpen}
        onClose={() => setUpdateCustomerDialogOpen(false)}
        customer={store.selectedCustomer}
        onUpdateCustomer={dispatchUpdateCustomer}
      />
      <DeleteCustomerDialog
        open={deleteCustomerDialogOpen}
        onClose={() => setDeleteCustomerDialogOpen(false)}
        onDeleteCustomer={dispatchDeleteCustomer}
      />

      <CustomSnackBar {...snackBarState} onClose={onCloseSnackBar} />
    </Box>
  );
};

export default App;
